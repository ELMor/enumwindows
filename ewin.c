
#include <windows.h>
#include <stdio.h>

static char **Apps;
static long index;
static long leido=0;

void copyDNS(char* dst,char* org){
	dst[0]=dst[1]=0;
	while( org[0] ){
		dst[0]=org[0];
		dst[1]=0;
		dst+=2;
		org+=1;
	}
}

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lpar){
	long size=GetWindowTextLength(hwnd);
	if(size){
		Apps=(char**)realloc(Apps,(1+index)*sizeof(char*));
		Apps[index]=(char*) malloc(1+size);
		GetWindowText(hwnd,Apps[index],size+1);
		index=index+1;
	}
	return TRUE;
}

int __declspec(dllexport) __stdcall AppTitle(long* i,char* Name){
	if(*i<0 || *i>=index)
		return 1;
	copyDNS(Name, Apps[*i]);
	return 0;
}

void __declspec(dllexport) __stdcall Fin(void){
	if(leido){
		long k;
		for(k=0;k<index;k++)
			free(Apps[k]);
		free(Apps);
	}
	Apps=NULL;
	index=0;
	leido=0;
}

int __declspec(dllexport) __stdcall Ini(void){
	Fin();
	EnumWindows(EnumWindowsProc,(LPARAM)NULL);
	leido=1;
	return index;
}

